import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import memoizeOne from 'memoize-one';
import Wrapper from '../components/Wrapper';
import { setActiveButton } from '../store/actions/app';
import Button from '../components/form/Button';
import { clearSingleGraph, getSingleGraphRequest } from '../store/actions/graphs';
import { userGraphRequest } from '../store/actions/shareGraphs';
import Api from '../Api';
import Header from '../components/Header';
import Select from '../components/form/Select';
import GraphCompareList from '../components/graphCompare/GraphCompareList';
import ChartUtils from '../helpers/ChartUtils';
import CreateGraphModal from '../components/CreateGraphModal';
import Utils from '../helpers/Utils';

class GraphCompare extends Component {
  static propTypes = {
    setActiveButton: PropTypes.func.isRequired,
    getSingleGraphRequest: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    singleGraph: PropTypes.object.isRequired,
  }

  getGraph1Request = memoizeOne(async (graphId) => {
    if (+graphId) {
      const { payload: { data = {} } } = await this.props.getSingleGraphRequest(graphId, { full: true });
      this.setState({ selectedNodes1: _.cloneDeep(ChartUtils.objectAndProto(data.graph?.nodes || [])) });
    }
  })

  getGraph2Request = memoizeOne(async (graph2Id) => {
    if (+graph2Id) {
      const { data = {} } = await Api.getSingleGraph(graph2Id, { full: true }).catch((e) => e);
      this.setState({ singleGraph2: data.graph || {}, selectedNodes2: _.cloneDeep(data.graph?.nodes || []) });
    }
  })

  constructor(props) {
    super(props);
    this.state = {
      singleGraph2: {},
      selectedNodes1: [],
      selectedNodes2: [],
    };
  }

  async componentDidMount() {
    const { match: { params: { graphId, graph2Id } } } = this.props;
    this.props.setActiveButton('view');
    this.props.clearSingleGraph();
  }

  loadGraphs = async (s) => {
    const { match: { params: { graphId, graph2Id } } } = this.props;
    const { data } = await Api.getGraphsList(1, {
      s,
      onlyTitle: 1,
    });
    const graphs = data.graphs
      .filter((g) => +g.id !== +graphId && +g.id !== +graph2Id)
      .map((g) => ({
        value: g.id,
        label: `${g.title} (${g.nodesCount})`,
      }));
    return graphs;
  }

  handleGraphSelect = (val, graph) => {
    const {
      match: { params: { graphId, graph2Id } },
    } = this.props;
    const { value = 0 } = val;
    if (graph === 1) {
      this.props.history.replace(`/graphs/compare/${value}/${+graph2Id || 0}`);
    } else {
      this.props.history.replace(`/graphs/compare/${+graphId || 0}/${value}`);
    }
  }

  handleChange = (d, checked, pos) => {
    const key = pos === 1 ? 'selectedNodes1' : 'selectedNodes2';
    const data = this.state[key];
    const i = data.findIndex((n) => n.id === d.id);
    if (checked) {
      if (i === -1) {
        data.push(d);
      }
    } else if (i > -1) {
      data.splice(i, 1);
    }
    this.setState({ [key]: data });
  }

  createGraph = () => {
    const { singleGraph } = this.props;
    const { singleGraph2 } = this.state;
    const { selectedNodes1, selectedNodes2 } = this.state;
    const createGraphData = ChartUtils.margeGraphs(singleGraph, singleGraph2, selectedNodes1, selectedNodes2);
    this.setState({ createGraphData });
  }

  closeCreateModal = () => {
    this.setState({ createGraphData: false });
  }

  renderSelectOption = (props) => {
    const {
      innerProps, children, getStyles, cx, ...params
    } = props;
    return (
      <div {...innerProps} className={cx(getStyles('option', params))}>{children}</div>
    );
  }

  render() {
    const {
      match: { params: { graphId, graph2Id } }, singleGraph,
    } = this.props;
    const {
      singleGraph2, selectedNodes1, selectedNodes2, createGraphData,
    } = this.state;
    this.getGraph1Request(graphId);
    this.getGraph2Request(graph2Id);
    const graph1Nodes = _.differenceBy(singleGraph.nodes, singleGraph2.nodes, 'name');
    const graph2Nodes = _.differenceBy(singleGraph2.nodes, singleGraph.nodes, 'name');

    const graph1CompareNodes = _.intersectionBy(singleGraph.nodes, singleGraph2.nodes, 'name');

    const selected = [...selectedNodes1, ...selectedNodes2];
    return (
      <Wrapper className="graphCompare" showFooter={false}>
        <Header />
        <div className="compareListWrapper">
          <ul className="compareList">
            <li className="item itemSearch">
              <div className="bottom">
                <div className="node node_left">
                  <Select
                    label="Graph 1"
                    isAsync
                    value={graphId && singleGraph.id ? [{
                      value: singleGraph.id,
                      label: `${singleGraph.title} (${singleGraph.nodes?.length})`,
                    }] : undefined}
                    onChange={(val) => this.handleGraphSelect(val, 1)}
                    loadOptions={this.loadGraphs}
                  />
                </div>
                <div className="node node_right">
                  <Select
                    label="Graph 2"
                    isAsync
                    value={graph2Id && singleGraph2.id ? [{
                      value: singleGraph2.id,
                      label: `${singleGraph2.title} (${singleGraph2.nodes?.length})`,
                    }] : undefined}
                    onChange={(val) => this.handleGraphSelect(val, 2)}
                    loadOptions={this.loadGraphs}
                  />
                </div>
              </div>

            </li>
          </ul>
          <GraphCompareList
            title={`Similar Nodes (${graph1CompareNodes.length}) `}
            singleGraph1={{ ...singleGraph, nodes: graph1CompareNodes }}
            singleGraph2={singleGraph2}
            onChange={this.handleChange}
            selected={selected}
          />
          <GraphCompareList
            title={(
              <span>
                {'Nodes in '}
                <strong>{singleGraph.title}</strong>
                {` (${graph1Nodes.length})`}
              </span>
            )}
            dropdown
            singleGraph1={{ ...singleGraph, nodes: graph1Nodes }}
            onChange={this.handleChange}
            selected={selected}
          />
          <GraphCompareList
            title={(
              <span>
                {'Nodes in '}
                <strong>{singleGraph2.title}</strong>
                {` (${graph2Nodes.length})`}
              </span>
            )}
            dropdown
            singleGraph2={{ ...singleGraph2, nodes: graph2Nodes }}
            onChange={this.handleChange}
            selected={selected}
          />
        </div>

        <Button onClick={this.createGraph} className="compareAndCreateNewGraph" color="main" icon="fa-plus">
          Create New Graph
        </Button>

        {!_.isEmpty(createGraphData) ? (
          <CreateGraphModal show data={createGraphData} onChange={this.closeCreateModal} />
        ) : null}
      </Wrapper>
    );
  }
}

const mapStateToProps = (state) => ({
  activeButton: state.app.activeButton,
  singleGraph: state.graphs.singleGraph,
  userGraphs: state.shareGraphs.userGraphs,
});
const mapDispatchToProps = {
  setActiveButton,
  getSingleGraphRequest,
  userGraphRequest,
  clearSingleGraph,
};
const Container = connect(
  mapStateToProps,
  mapDispatchToProps,
)(GraphCompare);

export default Container;
